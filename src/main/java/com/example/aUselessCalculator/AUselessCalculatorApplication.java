package com.example.aUselessCalculator;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sun.tools.jar.CommandLine;

@SpringBootApplication
public class AUselessCalculatorApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AUselessCalculatorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Calculator calc = new Calculator();
		calc.main();
	}
}

