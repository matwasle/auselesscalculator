package com.example.aUselessCalculator;

import java.util.Scanner;

public class Calculator {

    public void main() {
        int a,b,c;
        Scanner scan;

        scan = new Scanner(System.in);
        System.out.println("\n \n A useless Calculator \n");
        System.out.print("Bitte 1. Zahl eingeben: ");
        a = scan.nextInt();
        System.out.print("Bitte 2. Zahl eingeben: ");
        b = scan.nextInt();
        c = a + b;
        System.out.println("Summe beider Zahlen ist: "+c);
    }
}
